FROM rocker/r-base
MAINTAINER Andres Garcia <docker@ginkosan.com>

## Install dependencies
RUN apt-get update -qq && apt-get install -y \
  git-core \
  libssl-dev \
  libcurl4-gnutls-dev \
  libpq-dev

## Install R Packages for R API
RUN install2.r -e plumber jsonlite RPostgreSQL anytime xts lubridate

## Open #9000 port 
EXPOSE 9000

## Copy R Files
COPY . /R-Server
WORKDIR /R-Server

CMD ["R", "-f", "server.R"]

